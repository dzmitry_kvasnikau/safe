package com.safeapp;

import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import static org.junit.jupiter.api.Assertions.*;

class ResourcesTranslatorTest {

    @Test
    void translateToSubjects() {
        ResourceBundle resourceBundle = ResourceBundle.getBundle(Constants.PROPERTIES_FILE_NAME, Locale.getDefault());
        List<Subject> subjects = List.of(
                new Subject("keyboard", 5, 6),
                new Subject("microphone", 3, 4),
                new Subject("headphones", 1, 2),
                new Subject("mobile", 7, 10),
                new Subject("gamepad", 4, 7)
                );
        assertEquals(subjects, ResourcesTranslator.translateToSubjects(resourceBundle));
    }
}