package com.safeapp;

import org.junit.jupiter.api.Test;

import java.util.Locale;
import java.util.ResourceBundle;

import static org.junit.jupiter.api.Assertions.*;

class SubjectCreatorTest {

    @Test
    void createSubject() {
        ResourceBundle resourceBundle = ResourceBundle.getBundle(Constants.PROPERTIES_FILE_NAME, Locale.getDefault());
        Subject subject = SubjectCreator.createSubject(resourceBundle.getString("1"));
        assertEquals(new Subject("keyboard", 5, 6), subject);
    }
}