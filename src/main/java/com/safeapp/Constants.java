package com.safeapp;

public class Constants {
    public static final String PROPERTIES_FILE_NAME = "subjects";
    public static final String REGEX_SPLIT_PATTERN = ",";
    public static final String NULL_PARAMETERS_EXCEPTION_PHRASE = "None of parameters can be null";
    public static final String RESOURCES_TRANSLATOR_EXCEPTION_PHASE = "Resource bundle cannot be null";
    public static final String SUBJECT_CREATOR_EXCEPTION_PHASE = "Property cannot be null";
}
