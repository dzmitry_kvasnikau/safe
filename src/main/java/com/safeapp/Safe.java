package com.safeapp;

import lombok.Getter;
import lombok.ToString;
import java.util.ArrayList;
import java.util.List;

@Getter
@ToString
public class Safe {
    private final Integer volume;
    private final List<Subject> subjects;

    public Safe(Integer volume) {
        this.volume = volume;
        subjects = new ArrayList<>();
    }

    public void addSubject(Subject subject) {
        subjects.add(subject);
    }
}
