package com.safeapp;

import java.util.List;

public final class SubjectStacker {
    public static void stackSubjectsInSafe(List<Subject> subjects, Safe safe, int[][] priceRangeMatrix) {
        int tmpSafeShift = safe.getVolume();
        for (int i = subjects.size() - 1; i > 0; i--) {
            if (priceRangeMatrix[i][tmpSafeShift] != priceRangeMatrix[i - 1][tmpSafeShift]) {
                safe.addSubject(subjects.get(i));
                tmpSafeShift -= subjects.get(i).getVolume();
            }
        }
    }
}
