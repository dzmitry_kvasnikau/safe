package com.safeapp;

public final class SubjectCreator {
    public static Subject createSubject(String property) {
        String[] propertiesArray = property.split(Constants.REGEX_SPLIT_PATTERN);
        String name = propertiesArray[0];
        String volume = propertiesArray[1];
        String price = propertiesArray[2];

        return new Subject(name, Integer.parseInt(volume), Integer.parseInt(price));
    }
}
