package com.safeapp;

import java.util.List;

public final class MatrixCreator {
    public static int[][] createPriceRangeMatrix(Safe safe, List<Subject> subjects) {
        int[][] priceMatrix = new int[subjects.size()][safe.getVolume() + 1];
        for (int columnIndex = 0; columnIndex < safe.getVolume(); columnIndex++) {
            priceMatrix[0][columnIndex] = 0;
        }

        for (int lineIndex = 1; lineIndex < subjects.size(); lineIndex++) {
            for (int columnIndex = 0; columnIndex < safe.getVolume() + 1; columnIndex++) {
                if (columnIndex >= subjects.get(lineIndex).getVolume()) {
                    int currentMatrixElement = priceMatrix[lineIndex - 1][columnIndex];
                    int differenceMatrixElement = priceMatrix[lineIndex - 1][columnIndex - subjects.get(lineIndex).getVolume()] + subjects.get(lineIndex).getPrice();
                    priceMatrix[lineIndex][columnIndex] = Math.max(currentMatrixElement, differenceMatrixElement);
                } else {
                    priceMatrix[lineIndex][columnIndex] = priceMatrix[lineIndex - 1][columnIndex];
                }
            }
        }
        return priceMatrix;
    }
}
