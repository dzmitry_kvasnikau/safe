package com.safeapp;

import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

public class Starter {
    public static void start() {
        ResourceBundle resourceBundle = ResourceBundle.getBundle(Constants.PROPERTIES_FILE_NAME, Locale.getDefault());
        List<Subject> subjects = ResourcesTranslator.translateToSubjects(resourceBundle);
        Safe safe = new Safe(12);
        int[][] priceRangeMatrix = MatrixCreator.createPriceRangeMatrix(safe, subjects);
        SubjectStacker.stackSubjectsInSafe(subjects, safe, priceRangeMatrix);
        System.out.println(safe);
    }
}
