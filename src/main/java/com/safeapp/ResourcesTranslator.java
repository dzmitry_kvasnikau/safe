package com.safeapp;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.ResourceBundle;

public final class ResourcesTranslator {
    public static List<Subject> translateToSubjects(ResourceBundle resourceBundle) {
        List<Subject> subjectArray = new ArrayList<>();
        for (Enumeration<String> e = resourceBundle.getKeys(); e.hasMoreElements(); ) {
            String key = e.nextElement();
            Subject subject = SubjectCreator.createSubject(resourceBundle.getString(key));
            subjectArray.add(subject);
        }
        return subjectArray;
    }
}
